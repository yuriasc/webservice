<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        DB::table('users')->insert([
            'nome' => 'teste',
            'email' => 'teste@mail.com',
            'password' => app('hash')->make('123')
        ]);
    }
}
